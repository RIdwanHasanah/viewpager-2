package com.indonesia.ridwan.viewpagersecond;

/**
 * Created by hasanah on 9/12/16.
 */
public enum CustomPagerEnum {

    RED(R.string.red,R.layout.view_red);
    BLUE(R.string.blue,R.layout.view_blue);
    GREEn(R.string.green,R.layout.view_green);

    private int mTitleResid;
    private int mLayoutResId;

    CustomPagerEnum (int titleResid, int layoutResid){

        mTitleResid  = titleResid;
        mLayoutResId = layoutResid;
    }

    public int getmTitleResid(){

        return mTitleResid;
    }

    public int getmLayoutResId(){

        return mLayoutResId;
    }

}
