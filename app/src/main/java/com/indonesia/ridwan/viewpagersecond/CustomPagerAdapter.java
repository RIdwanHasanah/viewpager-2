package com.indonesia.ridwan.viewpagersecond;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

/**
 * Created by hasanah on 9/12/16.
 */
public class CustomPagerAdapter extends PagerAdapter {

    /**Atribute*/
    private Context mContext;

    /**Construktor*/
    public CustomPagerAdapter (Context context){

        mContext = context;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        /**Membuat objek baru dari CustomPagaerEnum */
        CustomPagerEnum customPagerEnum = CustomPagerEnum.values()[position];
        LayoutInflater inflater = LayoutInflater.from(mContext);
        ViewGroup layout = (ViewGroup) inflater.inflate(customPagerEnum.getmLayoutResId(),container,false);
        container.addView(layout);
        return layout;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }

    @Override
    public int getCount() {
        return CustomPagerEnum.values().length;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        CustomPagerEnum customPagerEnum = CustomPagerEnum.values()[position];
        return mContext.getString(customPagerEnum.getmTitleResid());
    }
}
